CREATE TABLE IF NOT EXISTS `advisor` (
`id` VARCHAR(512) NOT NULL ,
`name` VARCHAR(80) NOT NULL ,
`description` TEXT NULL ,
`availability` INT NOT NULL ,
`price_per_minute` FLOAT NOT NULL ,
`languages` VARCHAR(512) NOT NULL,
`profile_image` VARCHAR(512) NOT NULL,
PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
