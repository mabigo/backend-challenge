<?php
return [
    'statuslib' => [
        'db' => 'Mysqli',
        'table' => 'advisor',
    ],
    'api-tools-mvc-auth' => [
        'authentication' => [
            'adapters' => [
                'advisorservice' => [
                    'adapter' => \Laminas\ApiTools\MvcAuth\Authentication\HttpAdapter::class,
                    'options' => [
                        'accept_schemes' => [
                            0 => 'basic',
                        ],
                        'realm' => 'api',
                        'htpasswd' => 'data/htpasswd',
                    ],
                ],
            ],
        ],
    ],
    'db' => [
        'adapters' => [
            'Mysqli' => [
                'database' => 'backend-challenge',
                'driver'   => 'PDO_Mysql',
                'hostname' => 'db',
                'username' => 'user',
                'password' => '4hsnf1zgmxrBDEGtdK63',
            ],
        ],
    ],
];
