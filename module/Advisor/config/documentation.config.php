<?php
return [
    'Advisor\\V1\\Rest\\Advisor\\Controller' => [
        'description' => 'Create, manipulate, and retrieve advisor data',
        'collection' => [
            'description' => 'Manipulate lists of advisors.',
            'GET' => [
                'description' => 'Retrieve a paginated list of advisors.',
                'response' => '{
   "_links": {
       "self": {
           "href": "/advisor"
       },
       "first": {
           "href": "/advisor?page={page}"
       },
       "prev": {
           "href": "/advisor?page={page}"
       },
       "next": {
           "href": "/advisor?page={page}"
       },
       "last": {
           "href": "/advisor?page={page}"
       }
   }
   "_embedded": {
       "advisor": [
           {
               "_links": {
                   "self": {
                       "href": "/advisor[/:advisor_id]"
                   }
               }
              "Name": "The name of the advisor",
              "Description": "A description of the advisor",
              "Availability": "The current availability of the advisor",
              "Price per minute": "What you bill per minute to the customer",
              "Languages": "The languages the advisor speaks, at least one",
              "Profile Image": "An image up to 2 MByte"
           }
       ]
   }
}',
            ],
            'POST' => [
                'description' => 'Create a new advisors.',
                'request' => '{
   "Name": "The name of the advisor",
   "Description": "A description of the advisor",
   "Availability": "The current availability of the advisor",
   "Price per minute": "What you bill per minute to the customer",
   "Languages": "The languages the advisor speaks, at least one",
   "Profile Image": "An image up to 2 MByte"
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/advisor[/:advisor_id]"
       }
   }
   "Name": "The name of the advisor",
   "Description": "A description of the advisor",
   "Availability": "The current availability of the advisor",
   "Price per minute": "What you bill per minute to the customer",
   "Languages": "The languages the advisor speaks, at least one",
   "Profile Image": "An image up to 2 MByte"
}',
            ],
        ],
        'entity' => [
            'description' => 'Manipulate and retrieve individual advisor.',
            'PUT' => [
                'description' => 'Replace an advisor record.',
                'request' => '{
   "Name": "The name of the advisor",
   "Description": "A description of the advisor",
   "Availability": "The current availability of the advisor",
   "Price per minute": "What you bill per minute to the customer",
   "Languages": "The languages the advisor speaks, at least one",
   "Profile Image": "An image up to 2 MByte"
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/advisor[/:advisor_id]"
       }
   }
   "Name": "The name of the advisor",
   "Description": "A description of the advisor",
   "Availability": "The current availability of the advisor",
   "Price per minute": "What you bill per minute to the customer",
   "Languages": "The languages the advisor speaks, at least one",
   "Profile Image": "An image up to 2 MByte"
}',
            ],
            'GET' => [
                'description' => 'Retrieve an advisor record.',
                'response' => '{
   "_links": {
       "self": {
           "href": "/advisor[/:advisor_id]"
       }
   }
   "Name": "The name of the advisor",
   "Description": "A description of the advisor",
   "Availability": "The current availability of the advisor",
   "Price per minute": "What you bill per minute to the customer",
   "Languages": "The languages the advisor speaks, at least one",
   "Profile Image": "An image up to 2 MByte"
}',
            ],
            'PATCH' => [
                'description' => 'Update an advisor record.',
                'request' => '{
   "Name": "The name of the advisor",
   "Description": "A description of the advisor",
   "Availability": "The current availability of the advisor",
   "Price per minute": "What you bill per minute to the customer",
   "Languages": "The languages the advisor speaks, at least one",
   "Profile Image": "An image up to 2 MByte"
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/advisor[/:advisor_id]"
       }
   }
   "Name": "The name of the advisor",
   "Description": "A description of the advisor",
   "Availability": "The current availability of the advisor",
   "Price per minute": "What you bill per minute to the customer",
   "Languages": "The languages the advisor speaks, at least one",
   "Profile Image": "An image up to 2 MByte"
}',
            ],
            'DELETE' => [
                'description' => 'Delete an advisor record.',
                'request' => '{
   "Name": "The name of the advisor",
   "Description": "A description of the advisor",
   "Availability": "The current availability of the advisor",
   "Price per minute": "What you bill per minute to the customer",
   "Languages": "The languages the advisor speaks, at least one",
   "Profile Image": "An image up to 2 MByte"
}',
                'response' => '{
   "_links": {
       "self": {
           "href": "/advisor[/:advisor_id]"
       }
   }
   "Name": "The name of the advisor",
   "Description": "A description of the advisor",
   "Availability": "The current availability of the advisor",
   "Price per minute": "What you bill per minute to the customer",
   "Languages": "The languages the advisor speaks, at least one",
   "Profile Image": "An image up to 2 MByte"
}',
            ],
        ],
    ],
];
