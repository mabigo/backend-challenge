<?php
return [
    'service_manager' => [
        'aliases' => [
            Mapper::class => Advisor\Model\TableGatewayMapper::class,
        ],
        'factories' => [
            \Advisor\V1\Rest\Advisor\AdvisorResource::class => \Advisor\V1\Rest\Advisor\AdvisorResourceFactory::class,
            \Advisor\Model\TableGatewayMapper::class        => \Advisor\Model\TableGatewayMapperFactory::class,
            \Advisor\Model\TableGateway::class              => \Advisor\Model\TableGatewayFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'advisor.rest.advisor' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/advisor[/:advisor_id]',
                    'defaults' => [
                        'controller' => 'Advisor\\V1\\Rest\\Advisor\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'api-tools-versioning' => [
        'uri' => [
            0 => 'advisor.rest.advisor',
        ],
    ],
    'api-tools-rest' => [
        'Advisor\\V1\\Rest\\Advisor\\Controller' => [
            'listener' => \Advisor\V1\Rest\Advisor\AdvisorResource::class,
            'route_name' => 'advisor.rest.advisor',
            'route_identifier_name' => 'advisor_id',
            'collection_name' => 'advisor',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \Advisor\V1\Rest\Advisor\AdvisorEntity::class,
            'collection_class' => \Advisor\V1\Rest\Advisor\AdvisorCollection::class,
            'service_name' => 'Advisor',
        ],
    ],
    'api-tools-content-negotiation' => [
        'controllers' => [
            'Advisor\\V1\\Rest\\Advisor\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'Advisor\\V1\\Rest\\Advisor\\Controller' => [
                0 => 'application/vnd.advisor.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'Advisor\\V1\\Rest\\Advisor\\Controller' => [
                0 => 'application/vnd.advisor.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'api-tools-hal' => [
        'metadata_map' => [
            \Advisor\V1\Rest\Advisor\AdvisorEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'advisor.rest.advisor',
                'route_identifier_name' => 'advisor_id',
                'hydrator' => \Laminas\Hydrator\ObjectPropertyHydrator::class,
            ],
            \Advisor\V1\Rest\Advisor\AdvisorCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'advisor.rest.advisor',
                'route_identifier_name' => 'advisor_id',
                'is_collection' => true,
            ],
        ],
    ],
    'api-tools-content-validation' => [
        'Advisor\\V1\\Rest\\Advisor\\Controller' => [
            'input_filter' => 'Advisor\\V1\\Rest\\Advisor\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'Advisor\\V1\\Rest\\Advisor\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\StringLength::class,
                        'options' => [
                            'max' => '80',
                        ],
                    ],
                ],
                'filters' => [],
                'name' => 'name',
                'description' => 'The name of the advisor',
                'field_type' => '',
            ],
            1 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'description',
                'allow_empty' => true,
                'description' => 'A description of the advisor',
            ],
            2 => [
                'required' => false,
                'validators' => [],
                'filters' => [],
                'name' => 'availability',
                'field_type' => '',
                'description' => 'The current availability of the advisor',
            ],
            3 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'price_per_minute',
                'description' => 'What you bill per minute to the customer',
            ],
            4 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'languages',
                'description' => 'The languages the advisor speaks, at least one',
            ],
            5 => [
                'required' => false,
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\File\FilesSize::class,
                        'options' => [
                            'enableHeaderCheck' => true,
                            'max' => '2000',
                        ],
                    ],
                ],
                'filters' => [],
                'name' => 'profile_image',
                'description' => 'An image up to 2 MByte',
                'type' => \Laminas\InputFilter\FileInput::class,
            ],
        ],
    ],
    'api-tools-mvc-auth' => [
        'authorization' => [
            'Advisor\\V1\\Rest\\Advisor\\Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => true,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => true,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ],
            ],
        ],
    ],
];
