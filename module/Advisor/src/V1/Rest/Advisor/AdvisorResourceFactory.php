<?php
namespace Advisor\V1\Rest\Advisor;

use Mapper;

class AdvisorResourceFactory
{
    public function __invoke($services)
    {
        return new AdvisorResource($services->get(Mapper::class));
    }
}
