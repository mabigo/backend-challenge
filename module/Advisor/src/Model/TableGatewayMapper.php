<?php
namespace Advisor\Model;

use DomainException;
use InvalidArgumentException;
use Laminas\Paginator\Adapter\DbTableGateway;
use Laminas\Stdlib\ArrayUtils;
use Rhumsaa\Uuid\Uuid;
use Traversable;
use Advisor\Model\TableGateway;

/**
 * Mapper implementation using a Laminas\Db\TableGateway
 */
class TableGatewayMapper implements MapperInterface
{
    /**
     * @var TableGateway
     */
    protected $table;

    /**
     * @param TableGateway $table
     */
    public function __construct(TableGateway $table)
    {
        $this->table = $table;
    }

    /**
     * @param array|Traversable|\stdClass $data
     * @return Entity
     */
    public function create($data)
    {
        if ($data instanceof Traversable) {
            $data = ArrayUtils::iteratorToArray($data);
        }
        if (is_object($data)) {
            $data = (array) $data;
        }

        if (! is_array($data)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid data provided to %s; must be an array or Traversable',
                __METHOD__
            ));
        }

        $data['id'] = Uuid::uuid4()->toString();
        
        // This is obviously just a pragmatic way to avoid storing base64 data inside the database.
        // TODO: Implement custom filters classes for each 'languages' and 'profile_image' fields
        $data['languages'] = json_encode($data['languages']);
        $data['profile_image'] = $this->saveImage($data['id'], $data['profile_image']);
        
        $this->table->insert($data);

        $resultSet = $this->table->select(['id' => $data['id']]);
        if (0 === count($resultSet)) {
            throw new DomainException('Insert operation failed or did not result in new row', 500);
        }
        return $resultSet->current();
    }

    /**
     * @param string $id
     * @return Entity
     */
    public function fetch($id)
    {
        if (! Uuid::isValid($id)) {
            throw new DomainException('Invalid identifier provided', 404);
        }

        $resultSet = $this->table->select(['id' => $id]);
        if (0 === count($resultSet)) {
            throw new DomainException('Data not found', 404);
        }
        
        $result = $resultSet->current();

        $result->languages     = json_decode($result->languages);
        $result->profile_image = $this->getImage($result->profile_image);
 
        return $result;
    }

    /**
     * @param array $criteria
     * @return Collection
     */
    public function fetchAll($criteria = [])
    {
        //>>>TODO Implement filter and sorting by given criteria
        $collection = new Collection(new DbTableGateway($this->table, null, []) );

        $modifiedCollection = array();
        foreach($collection as $item){
            $item->languages     = json_decode($item->languages);
            $item->profile_image = $this->getImage($item->profile_image);
            
            $modifiedCollection[] = $item;
        }
        
        return $modifiedCollection;
    }

    /**
     * @param string $id
     * @param array|Traversable|\stdClass $data
     * @return Entity
     */
    public function update($id, $data)
    {
        if (! Uuid::isValid($id)) {
            throw new DomainException('Invalid identifier provided', 404);
        }
        if (is_object($data)) {
            $data = (array) $data;
        }
        
        $data['languages'] = json_encode($data['languages']);
        $data['profile_image'] = $this->saveImage($data['id'], $data['profile_image']);

        $this->table->update($data, ['id' => $id]);

        $resultSet = $this->table->select(['id' => $id]);
        if (0 === count($resultSet)) {
            throw new DomainException('Update operation failed or result in row deletion', 500);
        }
        return $resultSet->current();
    }

    /**
     * @param string $id
     * @return bool
     */
    public function delete($id)
    {
        if (! Uuid::isValid($id)) {
            throw new DomainException('Invalid identifier provided', 404);
        }

        $result = $this->table->delete(['id' => $id]);

        if (! $result) {
            return false;
        }

        return true;
    }
    
    /**
     * Saves base62 encoded image data as a file
     * 
     * @param string $id
     * @param string $base64String
     * @return string
     */
    private function saveImage($id, $base64String){
        $fileName = $id.'.base64';
        
        // image location path
        $filePath = __DIR__.'/../../../../data/images/'.$fileName;
        
        // Save image
        file_put_contents($filePath, $base64String);
        
        return $fileName;
    }
    
    /**
     * Gets base64 encoded advisor image from filesystem by filename
     * 
     * @param string $fileName
     * @return string
     */
    private function getImage($fileName){
        $filePath = __DIR__.'/../../../../data/images/'.$fileName;
        
        $data = file_get_contents($filePath);

        return $data;
    }
}
