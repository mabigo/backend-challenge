<?php
namespace Advisor\Model;

class Entity
{
    /**
     * @var string
     */
    public $id;
    
    /**
     * @var string
     */
    public $name;
    
    /**
     * @var string
     */
    public $description;
    
    /**
     * @var string
     */
    public $availability;
    
    /**
     * @var string
     */
    public $price_per_minute;
    
    /**
     * @var string
     */
    public $languages;
    
    /**
     * @var string
     */
    public $profile_image;
}
