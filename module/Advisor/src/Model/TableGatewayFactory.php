<?php
namespace Advisor\Model;

use DomainException;
use Advisor\Model\TableGateway;

/**
 * Service factory for the TableGateway
 *
 * If the "statuslib" key is present, and either the "db" or "table" subkeys
 * are present and valid, uses those; otherwise, uses defaults of "backend-challenge"
 * and "advisor", respectively.
 *
 * If the DB service does not exist, raises an error.
 *
 * Otherwise, creates a TableGateway instance with the DB service and table.
 */
class TableGatewayFactory
{
    public function __invoke($services)
    {
        $db    = 'backend-challenge';
        $table = 'advisor';
        if ($services->has('config')) {
            $config = $services->get('config');
            switch (isset($config['statuslib'])) {
                case true:
                    $config = $config['statuslib'];
                    $db     = isset($config['db']) ? $config['db'] : $db;
                    $table  = isset($config['table']) ? $config['table'] : $table;
                    break;
                case false:
                default:
                    break;
            }
        }

        if (! $services->has($db)) {
            throw new DomainException(sprintf(
                'Unable to create %s due to missing "%s" service',
                TableGateway::class,
                $db
            ));
        }

        return new TableGateway($table, $services->get($db));
    }
}
