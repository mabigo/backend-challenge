<?php
namespace Advisor\Model;

use DomainException;
use Advisor\Model\TableGatewayMapper;
use Advisor\Model\TableGateway;

/**
 * Service factory for returning a Advisor\Model\TableGatewayMapper instance.
 *
 * Requires the Advisor\Model\TableGateway service be present in the service locator.
 */
class TableGatewayMapperFactory
{
    public function __invoke($services)
    {
        if (! $services->has('Advisor\Model\TableGateway')) {
            throw new DomainException(sprintf(
                'Cannot create %s; missing %s dependency',
                TableGatewayMapper::class,
                TableGateway::class
            ));
        }
        return new TableGatewayMapper($services->get(TableGateway::class));
    }
}
