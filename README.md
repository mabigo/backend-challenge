Advisor REST API
======================================

Installation
------------

### Via Git (clone)

First, clone the repository:

```bash
# git clone git@bitbucket.org:mabigo/backend-challenge.git # optionally, specify the directory in which to clone
$ cd path/to/install
```

At this point, you need to use [Composer](https://getcomposer.org/) to install
dependencies. Assuming you already have Composer:

```bash
$ composer install
```

### Docker

If you develop or deploy using Docker, we provide configuration for you.

Prepare your development environment using [docker compose](https://docs.docker.com/compose/install/):

```bash
$ git clone git@bitbucket.org:mabigo/backend-challenge.git
$ cd backend-challenge
$ docker-compose build
```

Start the container:

```bash
$ docker-compose up
```

You may also use the provided `Dockerfile` directly if desired.

Access the REST API from `http://localhost:8080/advisor` or `http://<boot2docker ip>:8080/advisor` if on Windows or Mac.


REST API development state
--------
At the current state, API provides only basic features to create, delete, update and read advisor data.

TODO:

*  Implement filtering and sorting
*  Provide detailed validators for each advisor field
*  Optimize image storage and implement it as a custom filter class
*  Implement image processing as a custom filter class
*  Optimize bootstrap frontend for full server-side processing and replace hardcoded api credentials with configuration


Possible issues during local installation
--------

In case if Docker fails to automatically import the the database structure, you may need to import it manually.

```bash
$ bin/web-container.sh
$ mysql mysql -uuser -p -hdb backend-challenge < data/schema.sql
```

You may find mysql credentials in config/autoload/local.php


Front-end
--------
You may find a minimalistic Bootstrap Front-end for the API at `http://localhost:8080/`
The Frontend is just a pragmatic example and not meant to fully represent all features. 
Some limitations include: not being able to edit records, lack of server-side processing for pagination and so on.


QA Tools
--------

The Package ships with minimal QA tooling by default, including
laminas/laminas-test. 

```bash
$ composer require --dev squizlabs/php_codesniffer
```

There is an alias for the QA in the Composer configuration:

```bash
# Run PHPUnit tests:
$ composer test
```
